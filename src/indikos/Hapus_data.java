/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indikos;

import java.sql.Connection;
import java.sql.Statement;
import javax.swing.JFrame;
import sounds.SoundClick;

/**
 *
 * @author retina
 */
public class Hapus_data extends javax.swing.JFrame {
    private Connection konek;
    private Statement stm;
    /**
     * Creates new form Tambah
     */
    public Hapus_data() {
        
            initComponents();
            setTitle("Hapus Data");
            setLocationRelativeTo(null);
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            setResizable(false);
         
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        hapusBayarBtn = new javax.swing.JButton();
        hapusMemberBtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        Bg = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setPreferredSize(new java.awt.Dimension(400, 300));
        jPanel1.setLayout(null);

        hapusBayarBtn.setText("Hapus Data Bayar");
        hapusBayarBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                hapusBayarBtnMouseClicked(evt);
            }
        });
        jPanel1.add(hapusBayarBtn);
        hapusBayarBtn.setBounds(100, 150, 180, 40);

        hapusMemberBtn.setText("Hapus Data Member");
        hapusMemberBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                hapusMemberBtnMouseClicked(evt);
            }
        });
        jPanel1.add(hapusMemberBtn);
        hapusMemberBtn.setBounds(100, 80, 180, 40);

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("<html> <b>*Note :</b><br> \n<p>1. Hapus Data Member : untuk menghapus member data</p> \n<p>2. Hapus Data Bayaran : untuk menghapus data pembayaran pada tabel</p> \n</html>");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(20, 220, 360, 70);

        Bg.setBackground(new java.awt.Color(102, 0, 0));
        Bg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Project.png"))); // NOI18N
        Bg.setOpaque(true);
        jPanel1.add(Bg);
        Bg.setBounds(0, 0, 400, 300);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void hapusBayarBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_hapusBayarBtnMouseClicked
       new SoundClick().playSound();
       new Hapus_data_bayar().setVisible(true);
       dispose();  
      
       
    }//GEN-LAST:event_hapusBayarBtnMouseClicked

    private void hapusMemberBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_hapusMemberBtnMouseClicked
       new SoundClick().playSound();
       new Hapus_data_member().setVisible(true);
       dispose();
    }//GEN-LAST:event_hapusMemberBtnMouseClicked

    /**
     * @param args the command line arguments
     */


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Bg;
    private javax.swing.JButton hapusBayarBtn;
    private javax.swing.JButton hapusMemberBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
