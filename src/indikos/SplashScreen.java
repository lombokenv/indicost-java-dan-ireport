package indikos;

import javax.swing.JProgressBar;

public class SplashScreen extends javax.swing.JFrame {
 
    public SplashScreen() {
        initComponents();
        setLocationRelativeTo(null);  
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        progressbar = new javax.swing.JProgressBar();
        Cover = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        progressbar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        progressbar.setForeground(new java.awt.Color(0, 204, 0));
        progressbar.setStringPainted(true);
        jPanel1.add(progressbar, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 250, 540, 20));

        Cover.setBackground(new java.awt.Color(204, 51, 0));
        Cover.setForeground(new java.awt.Color(153, 0, 0));
        Cover.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/bgloader.png"))); // NOI18N
        Cover.setOpaque(true);
        jPanel1.add(Cover, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 590, 340));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    public static void main(String args[]) {

                SplashScreen sp = new SplashScreen(); 
                sp.setVisible(true);

                try{
                    for(int x=0;x<=100;x++){
                        Thread.sleep(50);
                        progressbar.setValue(x);
                    }
                    java.awt.EventQueue.invokeLater(new Runnable() {
                        public void run() {
                            new Home().setVisible(true);
                            sp.dispose();
                        }
                    });
                }catch(Exception e){
                    e.printStackTrace();
                }  
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Cover;
    private javax.swing.JPanel jPanel1;
    private static javax.swing.JProgressBar progressbar;
    // End of variables declaration//GEN-END:variables
}
