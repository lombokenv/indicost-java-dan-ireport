/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indikos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author retina
 */
public class Cetak extends javax.swing.JFrame {
    private Connection konek;
    private Statement stm,stm1;
    private boolean fillTanggal = false;
    private String tagihan;
    /**
     * Creates new form Tambah
     */
    public Cetak() {
        
            initComponents();
            setTitle("Cetak Data");
            setLocationRelativeTo(null);
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            setResizable(false);
            
            konek = new Koneksi().connect();
            tanggal_combobox.removeAllItems();
       
            getDataTanggal();   
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        saveBtn = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        tanggal_combobox = new javax.swing.JComboBox<>();
        Bg = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setPreferredSize(new java.awt.Dimension(400, 300));
        jPanel1.setLayout(null);

        saveBtn.setText("Cetak");
        saveBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                saveBtnMouseClicked(evt);
            }
        });
        jPanel1.add(saveBtn);
        saveBtn.setBounds(140, 120, 108, 40);

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Tanggal :");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(30, 40, 80, 15);

        tanggal_combobox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        tanggal_combobox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tanggal_comboboxActionPerformed(evt);
            }
        });
        jPanel1.add(tanggal_combobox);
        tanggal_combobox.setBounds(30, 60, 340, 30);

        Bg.setBackground(new java.awt.Color(218, 13, 13));
        Bg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/New Project.png"))); // NOI18N
        Bg.setOpaque(true);
        jPanel1.add(Bg);
        Bg.setBounds(0, 0, 400, 200);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void setTagihan(String tagihan){
        this.tagihan = tagihan;
    }
    public String getTagihan(){
        return this.tagihan;
    }
    private void saveBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveBtnMouseClicked
    konek = new Koneksi().connect();
    
        String getTanggal = tanggal_combobox.getSelectedItem().toString();
        String filePath = "src/Laporan/laporan.jrxml";
        try {
            
            Map<String,Object> parameter = new HashMap<String,Object>();
            parameter.put("tagihan","Rp"+getTagihan());
            
            JasperDesign jd = JRXmlLoader.load(filePath);
            String sql = "SELECT tanggal_bayar.tanggal,member.nama, member.no_hp, tanggal_bayar.nominal, tanggal_bayar.status_bayar FROM member JOIN tanggal_bayar ON member.id_member = tanggal_bayar.id_member WHERE tanggal_bayar.tanggal = '"+getTanggal+"'";
         
            JRDesignQuery jrd = new JRDesignQuery();
            jrd.setText(sql);
            jd.setQuery(jrd);
            
            JasperReport jr = JasperCompileManager.compileReport(jd);
            JasperPrint jp = JasperFillManager.fillReport(jr,parameter,konek );
            JasperViewer.viewReport(jp,false);
            
        } catch (JRException ex) {
            Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
        }      
          
       
    }//GEN-LAST:event_saveBtnMouseClicked

    private void tanggal_comboboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tanggal_comboboxActionPerformed
     
    }//GEN-LAST:event_tanggal_comboboxActionPerformed
  
  public void getDataTanggal(){
            try {
 
                String getCurrDate = "SELECT * FROM data_tanggal";
                stm = konek.createStatement();
                ResultSet rs1 = stm.executeQuery(getCurrDate);
            
            
            while(rs1.next()){
                tanggal_combobox.addItem(rs1.getString("tanggal_sekarang"));
            }
            
           
              
            } catch (SQLException ex) {
                 Logger.getLogger(Tambah.class.getName()).log(Level.SEVERE, null, ex);
            }      
  }
 
    /**
     * @param args the command line arguments
     */


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Bg;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton saveBtn;
    private javax.swing.JComboBox<String> tanggal_combobox;
    // End of variables declaration//GEN-END:variables
}
