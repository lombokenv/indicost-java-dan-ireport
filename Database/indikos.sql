-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 06, 2019 at 02:52 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `indikos`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_tanggal`
--

CREATE TABLE `data_tanggal` (
  `id` int(11) NOT NULL,
  `tanggal_sekarang` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_tanggal`
--

INSERT INTO `data_tanggal` (`id`, `tanggal_sekarang`) VALUES
(8, '2019-05-18'),
(14, '2019-05-21'),
(15, '2019-05-22'),
(16, '2019-06-04'),
(17, '2019-06-06');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id_member` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `no_hp` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id_member`, `nama`, `no_hp`) VALUES
(15, 'romy', '2222'),
(19, 'febrian guntur ilahi', '0819090000');

-- --------------------------------------------------------

--
-- Table structure for table `tanggal_bayar`
--

CREATE TABLE `tanggal_bayar` (
  `id_tanggal_bayar` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_member` int(11) NOT NULL,
  `nominal` int(15) DEFAULT NULL,
  `status_bayar` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tanggal_bayar`
--

INSERT INTO `tanggal_bayar` (`id_tanggal_bayar`, `tanggal`, `id_member`, `nominal`, `status_bayar`) VALUES
(30, '2019-05-18', 15, 35000, 'Cicilan'),
(31, '2019-05-18', 19, 30000, 'Sudah Bayar'),
(32, '2019-05-21', 15, 35000, 'Cicilan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_tanggal`
--
ALTER TABLE `data_tanggal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id_member`);

--
-- Indexes for table `tanggal_bayar`
--
ALTER TABLE `tanggal_bayar`
  ADD PRIMARY KEY (`id_tanggal_bayar`),
  ADD KEY `indikos` (`id_member`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_tanggal`
--
ALTER TABLE `data_tanggal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id_member` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tanggal_bayar`
--
ALTER TABLE `tanggal_bayar`
  MODIFY `id_tanggal_bayar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tanggal_bayar`
--
ALTER TABLE `tanggal_bayar`
  ADD CONSTRAINT `indikos` FOREIGN KEY (`id_member`) REFERENCES `member` (`id_member`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
